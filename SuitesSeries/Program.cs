﻿/************************************************************************************
 * Projet   : TP sur les Suites et les Séries
 * Auteur   : Ottavio Buonomo, Jorge José Goncalves, Cristiano Pereira Mendes
 * Desc.    : Calcule du taux d'intérêt et de la suite de Newton
 * Version  : 1.0
 * Date     : 14.06.2019
 ***********************************************************************************/

using System;
using System.IO;
using System.Collections.Generic;

namespace SuitesSeries
{
    class MainClass
    {
        const int MIN = -5;
        const int MAX = 5;

        public static void Main(string[] args)
        {
            // Exercice 1a
            Console.WriteLine("------------- Exercice 1a -------------" + Environment.NewLine);
            List<Fonction> suite = suiteNewton(1, 13);
            List<Fonction> funcNewton = getFonction(suite);
            List<Fonction> fX = new List<Fonction>();
            for (double i = MIN; i < MAX; i += 0.1)
            {
                Fonction f = new Fonction(i, getValueFromFomction(i));
                fX.Add(f);
            }
            createCSV(fX, "fonction.csv");
            createCSV(funcNewton, "fonctionNewton.csv");
            Console.WriteLine("CSV avec les valeurs crées avec succès" + Environment.NewLine);
            Console.WriteLine("------------- FIN Exercice 1 -------------" + Environment.NewLine);

            // Exercice 2
            Console.WriteLine("------------- Exercice 2 -------------");
            Console.WriteLine("------------- Partie 1 -------------");
            foreach (double item in calculateInterestPerYear(100000, 3, 6))
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("------------- Partie 2 -------------");

            foreach (double item in calculateInterestWithWithdrawal(100000, 20, 5, 10))
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("------------- Partie 3 -------------");
            int cpt = 1;
            foreach (List<double> item in evolutionInOneYear(100000, 3))
            {
                Console.WriteLine(cpt);
                foreach (double value in item)
                {
                    Console.WriteLine(value);
                }
                cpt++;
            }

            Console.WriteLine("------------- Partie 4 -------------");
            foreach (List<double> item in calculateInfinity(100000, 100, 1000000))
            {
                foreach (double value in item)
                {
                    Console.WriteLine(value);
                }
            }
            Console.WriteLine("------------- FIN Exercice 2 -------------" + Environment.NewLine);
        }

        /// <summary>
        /// Calcul l'image de X par une fonction
        /// </summary>
        /// <returns>Image de X.</returns>
        /// <param name="value">X.</param>
        public static double getValueFromFomction(double value)
        {
            return Math.Pow(value, 2) + 2;
        }

        public static List<Fonction> getFonction(List<Fonction> s)
        {
            List<Fonction> fonctions = new List<Fonction>();
            foreach (Fonction item in s)
            {
                Fonction f = new Fonction(item.Y, getValueFromFomction(item.Y));
                fonctions.Add(f);
            }
            return fonctions;
        }

        /// <summary>
        /// Calcul derivée.
        /// </summary>
        /// <returns>Derivée</returns>
        /// <param name="point">Point</param>
        public static Fonction calculeDerivee(double point)
        {
            double h = 0.00001;
            double yC = 0.0;

            Fonction valeursFonctionDerivee = new Fonction(0.0, 0.0);
            yC = (getValueFromFomction(point + h) - getValueFromFomction(point)) / h;
            valeursFonctionDerivee.X = point;
            valeursFonctionDerivee.Y = yC;
            return valeursFonctionDerivee;
        }


        /// <summary>
        /// Calcule la suite de Newton entre deux bornes
        /// </summary>
        /// <returns>Suite de Newton</returns>
        /// <param name="min">Minimum.</param>
        /// <param name="max">Max.</param>
        public static List<Fonction> suiteNewton(int min, int max)
        {
            double[] u = new double[max - min];
            List<Fonction> fonctions = new List<Fonction>();
            Random rng = new Random();
            u[0] = rng.Next(min, max + 1);
            for (int i = 0; i < max - min -1; i++)
            {
                u[i + 1] = u[i] - (getValueFromFomction(u[i]) / calculeDerivee(u[i]).Y);
                Fonction f = new Fonction(min + i, u[i]);
                fonctions.Add(f);
            }
            return fonctions;
        }

        /// <summary>
        /// Création de CSV.
        /// </summary>
        /// <param name="valeurs">Valeurs à écrire.</param>
        /// <param name="name">Nom du fichier.</param>
        public static void createCSV(List<Fonction> valeurs, string name)
        {
            string[] lines = new string[valeurs.Count];
            for (int i = 0; i < lines.Length; i++)
            {
                for (int j = 0; j < valeurs.Count; j++)
                {
                    lines[j] = String.Format("{0};{1}", valeurs[j].X.ToString().Replace('.', ','), valeurs[j].Y.ToString().Replace('.', ','));
                }
            }

            string docPath = "./";

            using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, name)))
            {
                foreach (string line in lines)
                    outputFile.WriteLine(line);
            }
        }

        /// <summary>
        /// Calcul l'intétêt sur un nombre d'année
        /// </summary>
        /// <returns>The interest per year.</returns>
        /// <param name="capital">Capital.</param>
        /// <param name="percentage">Taux d'intérêt.</param>
        /// <param name="nbOfYears">Nb d'années.</param>
        public static List<double> calculateInterestPerYear(double capital, double percentage, double nbOfYears)
        {
            List<double> interest = new List<double>();
            for (int i = 0; i <= nbOfYears; i++)
            {
                interest.Add(capital * Math.Pow(1 + (percentage / 100), i));
            }
            return interest;
        }

        /// <summary>
        /// Calcul l'intétêt sur un nombre d'année avec des retraits
        /// </summary>
        /// <returns>The interest per year.</returns>
        /// <param name="capital">Capital.</param>
        /// <param name="percentage">Taux d'intérêt.</param>
        /// <param name="nbOfYears">Nb d'années.</param>
        /// <param name="nbWithdrawal">Nb de retraits.</param>
        public static List<double> calculateInterestWithWithdrawal(double capital, double percentage, double nbOfYears, double nbWithdrawal)
        {
            List<double> interest = new List<double>();
            for (int i = 1; i <= nbWithdrawal + 1; i++)
            {
                capital = capital * Math.Pow(1 + ((percentage / 100) / (nbWithdrawal + 1)), nbOfYears);
                interest.Add(capital);
            }
            return interest;
        }

        public static List<List<double>> evolutionInOneYear(double capital, double percentage)
        {
            List<double> interest = new List<double>();
            List<List<double>> nbWithdrawalPerYear = new List<List<double>>();
            double tmpInterest = capital;
            for (int i = 1; i <= 365; i++)
            {
                for (int j = 1; j <= i + 1; j++)
                {
                    double nbWithdrawal = (percentage / 100) / (i + 1);
                    tmpInterest = tmpInterest * Math.Pow(1 + nbWithdrawal, 1);
                    interest.Add(tmpInterest);
                }
                nbWithdrawalPerYear.Add(interest);
                interest = new List<double>();
                tmpInterest = capital;
            }

            return nbWithdrawalPerYear;
        }

        public static List<List<double>> calculateInfinity(double capital, double percentage, double nbWithdrawal)
        {
            List<double> interest = new List<double>();
            List<List<double>> nbWithdrawalPerYear = new List<List<double>>();
            double tmpInterest = capital;
            for (int i = 1; i <= nbWithdrawal + 1; i += 1000)
            {
                for (int j = 1; j <= i + 1; j++)
                {
                    double percentagePerWithdrawal = (percentage / 100) / (i + 1);
                    tmpInterest = tmpInterest * Math.Pow(1 + percentagePerWithdrawal, 1);
                    if (j == i + 1)
                    {
                        interest.Add(tmpInterest);
                    }
                }
                nbWithdrawalPerYear.Add(interest);
                interest = new List<double>();
                tmpInterest = capital;
            }
            return nbWithdrawalPerYear;
        }
    }
}
