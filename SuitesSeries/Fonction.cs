﻿/************************************************************************************
 * Projet   : TP sur les Suites et les Séries
 * Classe   : Fonction.cs
 * Auteur   : Ottavio Buonomo, Jorge José Goncalves, Cristiano Pereira Mendes
 * Desc.    : Gestion de fonction mathématique
 * Version  : 1.0
 * Date     : 14.06.2019
 ***********************************************************************************/

using System;
namespace SuitesSeries
{
    public class Fonction
    {
        private double _x;
        private double _y;

        public double X { get => _x; set => _x = value; }
        public double Y { get => _y; set => _y = value; }

        public Fonction(double aX, double aY)
        {
            this.X = aX;
            this.Y = aY;
        }

        public override string ToString()
        {
            return String.Format("({0} ; {1})", this.X, this.Y);
        }
    }
}
